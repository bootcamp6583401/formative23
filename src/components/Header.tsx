import React from "react"
import HeaderLinks from "./HeaderLinks"

export default function Header() {
  return (
    <header className="bg-[url('bg-header.png')] bg-bottom py-12">
      <div className="mx-auto container flex gap-4 items-center justify-center">
        <HeaderLinks links={["Home", "About", "Men"]} />
        <div>
          <img src="/logo.png" />
        </div>
        <HeaderLinks links={["Women", "Blog", "Content"]} />
      </div>
    </header>
  )
}
