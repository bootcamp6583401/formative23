import React from "react"

export default function Social({
  bgPos,
  width,
}: {
  bgPos: string
  width?: string
}) {
  return (
    <a
      className="bg-[url('icons.png')] h-[22px] w-[23px] block"
      style={{ backgroundPosition: bgPos, width }}
    ></a>
  )
}
