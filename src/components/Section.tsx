import React from "react"

export default function Section({
  title,
  description,
  buttonTxt,
}: {
  title: string
  description: string
  buttonTxt: string
}) {
  return (
    <article className="flex flex-col items-center justify-center gap-4">
      <header className="w-full py-2 text-center bg-[url('/bg-heading2.png')] bg-cover bg-center custom-font text-3xl text-white font-bold">
        {title}
      </header>
      <p className="text-sm text-center mx-6 text-[#717272]">{description}</p>
      <button className="text-xl custom-font px-4 py-2 bg-[#37444C] font-bold text-white rounded-md shadow-md">
        {buttonTxt}
      </button>
    </article>
  )
}
