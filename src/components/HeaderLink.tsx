import React from "react"

export default function HeaderLink({ text }: { text: string }) {
  return (
    <button className="hover:bg-[#383938] p-2 text-white hover:text-[#FDF4B0] font-bold text-2xl tracking-tighter">
      {text}
    </button>
  )
}
