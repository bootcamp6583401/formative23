import React from "react"

export default function Picture({ url }: { url: string }) {
  return <img src={url} className="w-full object-cover" />
}
