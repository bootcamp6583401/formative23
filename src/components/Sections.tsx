import React from "react"
import Section from "./Section"
import { useAppSelector } from "../app/hooks"
import { selectSections } from "../features/main/mainSlice"

export default function Sections() {
  const sections = useAppSelector(selectSections)

  return (
    <div className="bg-[#242424] py-6 ">
      <div className="mx-auto container max-w-[960px]">
        <div className="grid grid-cols-3 gap-4">
          {sections.map((s, i) => (
            <Section
              key={i}
              title={s.title}
              description={s.description}
              buttonTxt={s.buttonTxt}
            />
          ))}
        </div>
      </div>
    </div>
  )
}
