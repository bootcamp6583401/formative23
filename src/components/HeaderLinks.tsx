import React from "react"
import HeaderLink from "./HeaderLink"

export default function HeaderLinks({ links }: { links: string[] }) {
  return (
    <ul className="flex gap-4">
      {links.map(link => (
        <li key={link}>
          <HeaderLink text={link} />
        </li>
      ))}
    </ul>
  )
}
