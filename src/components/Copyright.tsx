import React from "react"

export default function Copyright() {
  return (
    <div className="bg-[#393939] text-gray-400 text-xs text-center py-1">
      Copyright
    </div>
  )
}
