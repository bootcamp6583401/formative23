import React from "react"
import Picture from "./Picture"
import { useAppSelector } from "../app/hooks"
import { selectImages } from "../features/main/mainSlice"

export default function Pictures() {
  const imgs = useAppSelector(selectImages)

  return (
    <section className="mx-auto container max-w-[960px] py-6">
      <header className="bg-[url('bg-heading.png')] custom-font bg-full py-2 bg-cover mb-6">
        <h2 className="text-3xl tracking-tighter text-[#FDF4B0] text-center">
          HOT SHIRTS FOR THIS MONTH
        </h2>
      </header>
      <div className="grid grid-cols-3 gap-4">
        {imgs.map((p, i) => (
          <Picture key={i} url={p.path} />
        ))}
      </div>
    </section>
  )
}
