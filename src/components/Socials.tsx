import React from "react"
import Social from "./Social"
import { useAppSelector } from "../app/hooks"
import { selectSocials } from "../features/main/mainSlice"

export default function Socials() {
  const socials = useAppSelector(selectSocials)

  return (
    <section className="bg-[url('/bg-footer.jpg')] pt-6 pb-8">
      <div className="mx-auto container max-w-[960px] flex gap-8">
        <div className="text-white">
          <div className="font-bold mb-4">GET WEEKLY NEWSLETTER</div>
          <div className="flex gap-2">
            <input className="rounded-md p-2" />
            <button className="px-4 py-2 bg-gradient-to-b from-gray-400 via-gray-600 to-gray-700 rounded-md">
              Subscribe
            </button>
          </div>
        </div>
        <div className="text-white">
          <div className="font-bold mb-4">LATEST POST</div>
          <p className="text-[#bfbfbf]">
            sdjfsdf sjdfsdjf lskdfklsj dflkjsd kljsdf lksd fkljsdl kj sdfsdf
          </p>
        </div>
        <div className="text-white">
          <div className="font-bold mb-4">FOLLOW US:</div>
          <div className="flex gap-2">
            {socials.map((s, i) => (
              <Social key={i} bgPos={s.bgPos} width={s.width} />
            ))}
          </div>
        </div>
      </div>
    </section>
  )
}
