import type { PayloadAction } from "@reduxjs/toolkit"
import { createAppSlice } from "../../app/createAppSlice"
import type { AppThunk } from "../../app/store"

export type IImage = {
  path: string
}

export type ISection = {
  title: string
  description: string
  buttonTxt: string
}

export type ISocial = {
  bgPos: string
  width?: string
}

export interface MainSliceState {
  images: IImage[]
  sections: ISection[]
  socials: ISocial[]
}

const initialState: MainSliceState = {
  images: [],
  sections: [],
  socials: [],
}

export const mainSlice = createAppSlice({
  name: "main",
  initialState,
  reducers: create => ({
    getData: create.asyncThunk(
      async () => {
        const sections = await fetch("http://localhost:3000/sections")
        const images = await fetch("http://localhost:3000/images")
        const socials = await fetch("http://localhost:3000/socials")

        return {
          sections: (await sections.json()) as ISection[],
          images: (await images.json()) as IImage[],
          socials: (await socials.json()) as ISocial[],
        }
      },
      {
        fulfilled: (state, action) => {
          state.sections = action.payload.sections
          state.images = action.payload.images
          state.socials = action.payload.socials
        },
      },
    ),
  }),
  selectors: {
    selectSocials: main => main.socials,
    selectImages: main => main.images,
    selectSections: main => main.sections,
  },
})

export const { getData } = mainSlice.actions

export const { selectImages, selectSocials, selectSections } =
  mainSlice.selectors
