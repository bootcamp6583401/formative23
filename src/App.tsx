import { useEffect } from "react"
import "./App.css"
import { useAppDispatch, useAppSelector } from "./app/hooks"
import Copyright from "./components/Copyright"
import Header from "./components/Header"
import Pictures from "./components/Pictures"
import Sections from "./components/Sections"
import Socials from "./components/Socials"
import { getData } from "./features/main/mainSlice"

const App = () => {
  const dispatch = useAppDispatch()
  useEffect(() => {
    dispatch(getData(null))
  }, [])
  return (
    <div className="min-h-screen bg-[url('/bg-body.gif')]">
      <Header />
      <Pictures />
      <Sections />
      <Socials />
      <Copyright />
    </div>
  )
}

export default App
